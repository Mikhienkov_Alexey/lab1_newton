﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Color = System.Drawing.Color;

namespace ComputingMethodsLab1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public partial class MainWindow : Window
    {
        private Chart chart;
        private ChartArea area;
        private double error = 0.000000001;
        private double[,] finalDiffMatrix;

        private double a, b, g, d, eps, delta, x_min = -10, x_max = 10, y_min = -10, y_max = 10;
        private int n;

        private Series fx;
        private Series pnx;
        private Series rnx;
        private Series dfx;
        private Series dpnx;
        
        public MainWindow()
        {
            InitializeComponent();
        }
        
        // Создание графика
        private void CreateChart()
        {
            chart = new Chart();
            area = new ChartArea("Default");
            chart.Width = 1000;
            chart.Height = 1000;
            chart.ChartAreas.Add(area);

            WindowsFormsHost formsHost = new WindowsFormsHost();
            formsHost.Child = chart;
            
            Panel.Children.Clear();
            Panel.Children.Add(formsHost);

            area.AxisX.Minimum = x_min;
            area.AxisX.Maximum = x_max;
            area.AxisY.Minimum = y_min;
            area.AxisY.Maximum = y_max;
            area.AxisX.Interval = (x_max - x_min) / 20;
            area.AxisY.Interval = (y_max - y_min) / 20;
        }

        private double readDouble(TextBox box, String name, double min = -100, double max = 100) {
            String boxText = box.Text;
            if (boxText.Equals("")) {
                return 0;
            }
            
            double value;
            bool success = Double.TryParse(boxText, out value);
            
            if (!success)
            {
                throw new Exception(name + " должно быть действительным числом");
            }

            if (!(value >= min && value <= max))
            {
                throw new Exception("Коэффициент " + name + " должен находиться в границах от " + min + " до " + max + "!");
            }

            return value;
        }
        
        // Обработка нажатия на кнопку "Применить"
        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            try {
                calculate();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void calculate() {
            a = readDouble(Alfa, "альфа");
            b = readDouble(Beta, "бэта");
            g = readDouble(Gamma, "гамма");
            d = readDouble(Delta, "дельта");
            eps = readDouble(Epsilon, "эпсилон");

            x_min = readDouble(minX, "минимум по X");
            x_max = readDouble(maxX, "максимум по X");
                
            y_min = readDouble(minY, "минимум по Y");
            y_max = readDouble(maxY, "максимум по Y");

            n = (int) readDouble(Count_N, "количество узлов", 0, 200);
            
            if (x_min >= x_max)
                throw new Exception("Минимальный параметр по X больше или равен максимальному");

            if (y_min >= y_max)
                throw new Exception("Минимальный параметр по Y больше или равен максимальному");

            this.delta = Convert.ToDouble(ComboBox.Text);

            CreateChart();

            if (FxCheckBox.IsChecked == true)
                Fx();
            else
                fx?.Points.Clear();

            if (PnxCheckBox.IsChecked == true)
                Pnx();
            else
                pnx?.Points.Clear();

            if (RnxCheckBox.IsChecked == true)
            {
                if (FxCheckBox.IsChecked == true && PnxCheckBox.IsChecked == true)
                    Rnx();
                else
                    MessageBox.Show("Сначала нужно построить график для Fx и Pnx");
            }
            else
            {
                rnx?.Points.Clear();
                Rejection.Text = "";
            }

            if (dFxCheckBox.IsChecked == true)
                dFx();
            else
                dfx?.Points.Clear();

            if (dPnxCheckBox.IsChecked == true)
            {
                if (FxCheckBox.IsChecked == true && PnxCheckBox.IsChecked == true)
                    dPnx();
                else
                    MessageBox.Show("Сначала нужно построить график для Fx и Pnx");
            }
            else
            {
                dpnx?.Points.Clear();
            }
            
        }

        // Исходная функция
        private void Fx()
        {
            fx = new Series("fx");
            fx.ChartArea = "Default";
            fx.ChartType = SeriesChartType.Line;
            fx.Color = Color.Green;
            fx.BorderWidth = 3;

            double x, y;

            int step = 0;

            finalDiffMatrix = new double[1000, 1000];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n + 1; j++)
                    finalDiffMatrix[i, j] = 0;

            for (x = x_min; x <= x_max; x += (x_max - x_min) / (n - 1))
            {

                if (Math.Abs(x - g) < error)
                    y = 0.0;
                else
                    y = a * Math.Sin(b / Math.Pow(x - g, 2)) + d * Math.Cos(eps * x);

                finalDiffMatrix[step, 0] = x;
                finalDiffMatrix[step, 1] = y;

                step++;
            }
            
            for (x = x_min; x <= x_max; x += (x_max - x_min) / 2000)
            {
                if (Math.Abs(x - g) < error)
                    y = 0.0;
                else
                    y = a * Math.Sin(b / Math.Pow(x - g, 2)) + d * Math.Cos(eps * x);

                fx.Points.Add(new DataPoint(x, y));
            }
            
            chart.Series.Add(fx);
        }

        // Интерполяционный полином Ньютона
        private void Pnx()
        {
            pnx = new Series("Pnx");
            pnx.ChartArea = "Default";
            pnx.ChartType = SeriesChartType.Line;
            pnx.Color = Color.Teal;
            pnx.BorderWidth = 3;

            int st = 0;
            double x, y;

            for (int j = 2; j < n + 1; j++)
            {
                for (int i = 0; i < n - 1 - st; i++)
                {
                    finalDiffMatrix[i, j] = finalDiffMatrix[i + 1, j - 1] - finalDiffMatrix[i, j - 1];
                }

                st++;
            }

            for (x = x_min; x <= x_max; x += (x_max - x_min) / (n - 1))
            {
                y = POLINOM(x, finalDiffMatrix, n, (x_max - x_min) / (n - 1));

                if (Math.Abs(y) >= 10000000)
                    y = 1000000;

                pnx.Points.Add(new DataPoint(x, y));
            }
            
            chart.Series.Add(pnx);
        }

        double POLINOM(double x, double[,] konRazn, Int64 n, double h)
        {
            double res = 0;
            double t = (x - x_min) / h;

            double chislitel = 1;
            res += konRazn[0, 1];

            for (int i = 2; i < n; i++)
            {
                chislitel = chislitel * (t - i + 1) / i;
                res += konRazn[0, i] * chislitel;
            }
            return res;
        }

        // Отклонение Pnx от Fx
        private void Rnx()
        {
            rnx = new Series("Rnx");
            rnx.ChartArea = "Default";
            rnx.ChartType = SeriesChartType.Line;
            rnx.Color = Color.Red;
            rnx.BorderWidth = 3;
            double pnx_x;
            double pnx_y;
            double cur_y, max_otklon = 0, x = 0;

            // Построение графика
            foreach (DataPoint dataPoint in pnx.Points)
            {
                pnx_x = (double)dataPoint.XValue;
                pnx_y = dataPoint.YValues.Length == 1 ? dataPoint.YValues.Max() : 0;

                if (Math.Abs(pnx_x - g) < error)
                    cur_y = 0.0;
                else
                    cur_y = a * Math.Sin(b / Math.Pow(pnx_x - g, 2)) + d * Math.Cos(eps * pnx_x);

                if (Math.Abs(pnx_y - cur_y) > max_otklon)
                {
                    if (Math.Abs(cur_y) >= 1000000)
                        max_otklon = 1000000;
                    else
                        max_otklon = Math.Abs(pnx_y - cur_y);
                    x = pnx_x;
                }
            }

            for (double y = y_min; y <= y_max; y += (y_max - y_min) / 10)
            {
                rnx.Points.Add(new DataPoint(x, y));
            }

            chart.Series.Add(rnx);

            String answer = "x = " + Math.Round(x, 3) + " \nmax|rn(x)| = " + Math.Round(max_otklon, 3);
            Rejection.Text = answer;
        }

        // Разностная производная вперед по Fx
        private void dFx()
        {
            dfx = new Series("dFx");
            dfx.ChartArea = "Default";
            dfx.ChartType = SeriesChartType.Line;
            dfx.Color = Color.HotPink;
            dfx.BorderWidth = 3;

            delta = Convert.ToDouble(ComboBox.Text);
            double x, y;

            for (x = x_min; x <= x_max; x += (x_max - x_min) / 2000)
            {
                if (Math.Abs((x + delta) - g) < error || Math.Abs(x - g) < error)
                    y = 0.0;
                else
                    y = ((a * Math.Sin(b / Math.Pow((x + delta) - g, 2)) + d * Math.Cos(eps * (x + delta))) - (a * Math.Sin(b / Math.Pow(x - g, 2)) + d * Math.Cos(eps * x))) / delta;
                dfx.Points.Add(new DataPoint(x, y));
            }

            chart.Series.Add(dfx);
        }

        // Разностная производная вперед по Pnx
        private void dPnx()
        {
            dpnx = new Series("dPnx");
            dpnx.ChartArea = "Default";
            dpnx.ChartType = SeriesChartType.Line;
            dpnx.Color = Color.HotPink;
            dpnx.BorderWidth = 3;
            double x, y;

            // Построение графика
            for (x = x_min; x <= x_max; x += (x_max - x_min) / (n - 1))
            {
                y = POLINOM(x, finalDiffMatrix, n, (x_max - x_min) / (n - 1)) - POLINOM(x + delta, finalDiffMatrix, n, (x_max - x_min) / (n - 1));

                if (Math.Abs(y) >= 10000000)
                    y = 1000000;

                dpnx.Points.Add(new DataPoint(x, y));
            }

            chart.Series.Add(dpnx);
        }

    }
}
